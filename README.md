# Handle Duplication in SQL Server

Different ways to handle duplication in SQL Server.

**CaseI:** Duplication based on firstname, lastname,emailaddress and zip code.

**CaseII:** Duplication based on Email Address.

**CaseIII:** Duplication based on typos in First Name.

**CaseIV:** Duplication based on typos in Email Address.

**CaseV:** Duplication based on typos in Company name. We will firstly
apply rollup and then will look for duplication.

**CaseVI:** Duplication based on typos in Address. We will firstly
apply rollup and then will look for duplication.

**CaseVII:** Duplication based on FullName and Phone No.

**CaseVIII:** Duplication based on FullName.

**CaseIX:** Duplication based on Email Domain.

**CaseX:** Duplication based on Domain.

**CaseXI:** Duplication based on Email Address (left character before @) 
and Full Name.
